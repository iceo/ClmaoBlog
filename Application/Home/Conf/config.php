<?php

return array(
//    'SHOW_PAGE_TRACE' => true, // 显示页面Trace信息
    //静态缓存设置
    'HTML_CACHE_ON' => true, // 开启静态缓存
    'HTML_CACHE_TIME' => 60, // 全局静态缓存有效期（秒）
    'HTML_READ_TYPE' => 0,
    'HTML_FILE_SUFFIX' => '.tpl', // 设置静态缓存文件后缀
    'HTML_CACHE_RULES' => array(// 定义静态缓存规则
    // 定义格式1 数组方式
        'content' => array('{:action}_{id}',-1),
    // 定义格式2 字符串方式
    ),
);
