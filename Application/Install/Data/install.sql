/*
Navicat MySQL Data Transfer

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2014-07-12 11:20:00
*/

SET FOREIGN_KEY_CHECKS=0;

--
-- 表的结构 `clmao_access`
--
DROP TABLE IF EXISTS `clmao_access`;
CREATE TABLE IF NOT EXISTS `clmao_access` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_category`
--
DROP TABLE IF EXISTS `clmao_category`;
CREATE TABLE IF NOT EXISTS `clmao_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `is_nav` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_content`
--
DROP TABLE IF EXISTS `clmao_content`;
CREATE TABLE IF NOT EXISTS `clmao_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT '未命名',
  `content` text,
  `time` int(20) DEFAULT '0',
  `c_id` int(11) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  `author` varchar(255) DEFAULT '撒哈拉的小猫',
  `excerpt` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_links`
--
DROP TABLE IF EXISTS `clmao_links`;
CREATE TABLE IF NOT EXISTS `clmao_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `is_where` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_node`
--
DROP TABLE IF EXISTS `clmao_node`;
CREATE TABLE IF NOT EXISTS `clmao_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `remark` varchar(255) DEFAULT NULL,
  `sort` smallint(6) unsigned DEFAULT NULL,
  `pid` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `clmao_node`
--
INSERT INTO `clmao_node` (`id`, `name`, `title`, `status`, `remark`, `sort`, `pid`, `level`) VALUES
(16, 'Home', '应用', 1, NULL, NULL, 0, 1),
(17, 'Admin', '文章管理', 1, NULL, NULL, 16, 2),
(18, 'Cache', '缓存管理', 1, NULL, NULL, 16, 2),
(19, 'Char', '图表统计', 1, NULL, NULL, 16, 2),
(20, 'Config', '在线编辑', 1, NULL, NULL, 16, 2),
(21, 'Data', '数据管理', 1, NULL, NULL, 16, 2),
(22, 'Freshen', '更新文档', 1, NULL, NULL, 16, 2),
(23, 'Links', '链接管理', 1, NULL, NULL, 16, 2),
(24, 'Media', '媒体管理', 1, NULL, NULL, 16, 2),
(25, 'Rbac', '权限管理', 1, NULL, NULL, 16, 2),
(26, 'SiteMap', '地图管理', 1, NULL, NULL, 16, 2),
(27, 'Siteoption', '站点信息管理', 1, NULL, NULL, 16, 2),
(28, 'Ueditor', '上传文件', 1, NULL, NULL, 16, 2),
(29, 'index', '站点信息', 1, NULL, NULL, 27, 3),
(30, '_initialize', '初始化', 1, NULL, NULL, 28, 3),
(31, 'index', '首页', 1, NULL, NULL, 28, 3),
(32, 'upFile', '上传', 1, NULL, NULL, 28, 3),
(33, 'upBase64', '上传B64文件', 1, NULL, NULL, 28, 3),
(34, 'saveRemote', '获取远程文件', 1, NULL, NULL, 28, 3),
(35, 'file_list', '文件列表', 1, NULL, NULL, 28, 3),
(36, 'getfiles', '得到文件', 1, NULL, NULL, 28, 3),
(37, 'formatUrl', '文件格式', 1, NULL, NULL, 28, 3),
(38, 'format_exts', '文件后缀', 1, NULL, NULL, 28, 3),
(39, 'createXML', '创建XML', 1, NULL, NULL, 26, 3),
(40, 'createHtml', '创建HTML', 1, NULL, NULL, 26, 3),
(41, 'index', '用户列表', 1, NULL, NULL, 25, 3),
(42, 'role', '角色列表', 1, NULL, NULL, 25, 3),
(43, 'node', '节点列表', 1, NULL, NULL, 25, 3),
(44, 'addUser', '添加用户视图', 1, NULL, NULL, 25, 3),
(45, 'addUserHandle', '添加用户处理', 1, NULL, NULL, 25, 3),
(46, 'addRole', '添加角色视图', 1, NULL, NULL, 25, 3),
(47, 'addRoleHandle', '添加角色处理', 1, NULL, NULL, 25, 3),
(48, 'addNode', '添加节点视图', 1, NULL, NULL, 25, 3),
(49, 'addNodeHandle', '添加节点处理', 1, NULL, NULL, 25, 3),
(50, 'access', '配置权限视图', 1, NULL, NULL, 25, 3),
(51, 'setAccess', '配置权限处理', 1, NULL, NULL, 25, 3),
(52, 'index', '媒体库', 1, NULL, NULL, 24, 3),
(53, 'del', '删除媒体', 1, NULL, NULL, 24, 3),
(54, 'add', '添加视图', 1, NULL, NULL, 24, 3),
(55, 'add_process', '添加媒体处理', 1, NULL, NULL, 24, 3),
(56, 'addLinks', '添加链接视图', 1, NULL, NULL, 23, 3),
(57, 'addLinksProcess', '添加链接处理', 1, NULL, NULL, 23, 3),
(58, 'listLinks', '链接列表', 1, NULL, NULL, 23, 3),
(59, 'delLinksProcess', '删除链接', 1, NULL, NULL, 23, 3),
(60, 'editLinks', '编辑链接视图', 1, NULL, NULL, 23, 3),
(61, 'editLinksProcess', '编辑链接处理', 1, NULL, NULL, 23, 3),
(62, 'index', '首页', 1, NULL, NULL, 22, 3),
(63, 'freshen', '更新文档', 1, NULL, NULL, 22, 3),
(64, 'backupDB', '备份数据', 1, NULL, NULL, 21, 3),
(65, 'sql', 'SQL命令表单', 1, NULL, NULL, 21, 3),
(66, 'opimize', '优化修复表', 1, NULL, NULL, 21, 3),
(67, 'config', '在线编辑', 1, NULL, NULL, 20, 3),
(68, 'delLinkeCache', '删除链接缓存', 1, NULL, NULL, 18, 3),
(69, 'delSiteOptionCache', '删除站点信息缓存', 1, NULL, NULL, 18, 3),
(70, 'content', '文章月份统计', 1, NULL, NULL, 19, 3),
(71, 'category', '文章分类统计', 1, NULL, NULL, 19, 3),
(72, 'adminIndex', '首页', 1, NULL, NULL, 17, 3),
(73, 'addContent', '添加文章视图', 1, NULL, NULL, 17, 3),
(74, 'addContentProcess', '添加文章处理', 1, NULL, NULL, 17, 3),
(75, 'delContentProcess', '删除文章', 1, NULL, NULL, 17, 3),
(76, 'listContent', '文章列表', 1, NULL, NULL, 17, 3),
(77, 'addDraftProcess', '保存草稿', 1, NULL, NULL, 17, 3),
(78, 'listDraft', '草稿列表', 1, NULL, NULL, 17, 3),
(79, 'editContent', '编辑内容', 1, NULL, NULL, 17, 3),
(80, 'editContentProcess', '编辑内容处理', 1, NULL, NULL, 17, 3),
(81, 'main', '系统信息', 1, NULL, NULL, 17, 3),
(82, 'statusContent', '回收发布状态', 1, NULL, NULL, 17, 3),
(83, 'listCallback', '回收站', 1, NULL, NULL, 17, 3),
(84, 'sale', '安全退出', 1, NULL, NULL, 17, 3),
(85, 'listCategory', '分类列表', 1, NULL, NULL, 17, 3),
(86, 'addCategory', '添加分类', 1, NULL, NULL, 17, 3),
(87, 'addCategoryProcess', '添加分类处理', 1, NULL, NULL, 17, 3),
(88, 'delCategoryProcess', '删除分类', 1, NULL, NULL, 17, 3),
(89, 'editCategory', '编辑分类视图', 1, NULL, NULL, 17, 3),
(90, 'editCategoryProcess', '编辑分类处理', 1, NULL, NULL, 17, 3),
(91, 'help', '关于', 1, NULL, NULL, 17, 3),
(92, 'pc', 'PC端', 1, NULL, NULL, 17, 3),
(93, 'mobile', '移动端', 1, NULL, NULL, 17, 3);

-- --------------------------------------------------------

--
-- 表的结构 `clmao_role`
--
DROP TABLE IF EXISTS `clmao_role`;
CREATE TABLE IF NOT EXISTS `clmao_role` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_role_user`
--
DROP TABLE IF EXISTS `clmao_role_user`;
CREATE TABLE IF NOT EXISTS `clmao_role_user` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_user`
--
DROP TABLE IF EXISTS `clmao_user`;
CREATE TABLE IF NOT EXISTS `clmao_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `identifier` char(32) DEFAULT NULL,
  `token` char(32) DEFAULT NULL,
  `timeout` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `clmao_option`
--
DROP TABLE IF EXISTS `clmao_option`;
CREATE TABLE IF NOT EXISTS `clmao_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `clmao_option`
--
INSERT INTO `clmao_option` (`id`, `key`, `value`) VALUES
(1, 'siteName', '我的站点呵呵'),
(2, 'homeKey', '首页关键字'),
(3, 'homeDsc', '首页描述'),
(4, 'homeTitle', '首页副标题'),
(5, 'defaultAuthor', '默认作者'),
(6, 'footerPlus', ''),
(7, 'headerPlus', '');