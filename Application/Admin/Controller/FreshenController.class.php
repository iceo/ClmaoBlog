<?php

namespace Admin\Controller;

use Think\Controller;

class FreshenController extends CommonController {

    public function index() {
        $this->title='文章缓存';
        $this->display();
    }

    /* 执行更新 */

    public function freshen() {
        set_time_limit(0);
        ob_start();
        $dopost = I('post.dopost');
        $id = I('post.id') + 0;
        $dir = './Application/Html';

        if ($dopost == 'one') {
            clmao_update_html($id);
            echo 'ID为'.$id.'的的文章已经更新';
        } else if ($dopost == 'all') {
            
            $all_id = D('content')->getAllID();

            foreach ($all_id as $v) {
                clmao_update_html($v);
                echo "更新ID[{$v}]的文章成功<br/>";
                ob_flush();
                flush();
                ob_end_flush();
                usleep(500);
            }
            echo '更新完成';
        }
    }

}
